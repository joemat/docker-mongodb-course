A docker image I prepared to get through the course https://university.mongodb.com/courses/M101J/about

It includes

* mongodb
* maven
* mongoProc
* all dependencies to run the above tools (including X11 libraries)

Start with

`bash
docker run --name mongodb-course -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY -v ~/development:/development joemat/docker-mongodb-course
`

MongoDB is started when the container is started.

Get into running container with (e.g. for compiling the examples):

`docker exec -ti mongodb-course bash`

To run the maven build for homework run this command within the project dir:

`docker run --rm -v $PWD:/Development joemat/docker-mongodb-course bash -c 'cd /Development; mvn clean compile exec:java -Dexec.mainClass=com.mongodb.SparkHomework'` 
